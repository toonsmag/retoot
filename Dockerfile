FROM ubuntu:xenial

WORKDIR /root
COPY requirements.txt .
COPY setup.py .
COPY launch.sh .
COPY retoot/* retoot/
RUN echo LANG=C.UTF-8 >> /etc/default/locale
RUN update-locale
RUN apt-get update
RUN apt-get install -y apt-utils
RUN apt-get install -y python3 virtualenv
RUN virtualenv -p python3 retoot_env
RUN bash -c "source retoot_env/bin/activate && python3 setup.py install"
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
CMD bash -c "./launch.sh"
